# View Guide User Study in VR 

- More information, please feel free to visit my personal website [here](https://hanhonglei.github.io/).
- This is the source project of the paper [Under the Movement of Head: Evaluating Visual Attention in Immersive Virtual Reality Environment](https://github.com/hanhonglei/VRViewGuide)

## User study scenes
There are five user study scenes developed with `Unity 3D` game engine. Each of them contains one or two objects with three addition problems, appearing sequentially, between two numbers less than ten for participants to solve. They all have the same scene arrangement. Each task-related object in the scenes is rotating. The differences are the number of task-related objects and their moving trajectories. Scene $1 static$ contains only one task-related object, while scene $2 static$ contains two. In order to find out if participants' view direction is strongly correlated to task-related objects, we set the objects to be active in the last three scenes. In $1 move$ scene, there is only one move task-related object moving horizontally, back and forth. The $2 moveS$ scene contains two in up-and-down and left-and-right trajectories, respectively. The final scene $2 move$ contains two moving task-related objects in triangular and rectangular trajectories, which are more complex compared to previous scenes. All five of these scenes contain several background objects that are motionless. They are used to compare attention differences with task-related objects. We hypothesized that participants in immersive VR scenes tend to orientate their heads towards the task-related objects, while simply scanning with their own eyeballs with a stable virtual camera in non-immersive scenes. 

## Personalized storyboard demo
We developed an automatic storyboard generation system. It's a immersive VR short film using resources from the cutting-edge demo Adam, shared by Unity. There is a realistic indoor scene that lasts for 100 seconds, where four animated characters will appear in the scene consecutively. 

A script was assigned to the particular parts of each character, and some background objects, in order to record their levels of attention. There is an accumulator parameter in the script to collect the attention value calculated by the equation proposed in the paper in each fixed frame. Once the accumulator exceeds the value 3, the current screen will be captured as a key frame. 

## Important notes
The user behavior and all necessary game objects gaze information will be outputted into the folder `Output`. File names will be marked a time prefix.

When debug source project using `Unity`, you should first create a folder in the root folder, and name it as `Output`. All game data files will be saved in this subfolder, but is not updated by `Github`.

When you run this application in release mode, you should create the `Output` folder inside `_Data` folder.

----

- This project is under [GNU GENERAL PUBLIC LICENSE](https://www.gnu.org/licenses/), please check it in the root folder.