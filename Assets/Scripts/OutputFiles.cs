﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

public class OutputFiles : MonoBehaviour
{
    private List<StreamWriter> _recordFiles;    // all necessary files used in this scene
    private string _prefix;                     // file name prefix

    private float _levelTime = 0.0f;            // current level time

    public StreamWriter _sceneStatistics;      // important statistics information in this scene
    //{
    //    get { return _sceneStatistics; }
    //    private set {; }
    //}

    public GameObject allObjects;       // the objects' parent object including all potential perceptive objects
    private Recorder[] allRecorders;


    // Awake will be called before Start function [10/28/2016 Han]
    // So use Awake initialize me, and Start to interact with the others
    void Awake()
    {
        GameObject lm = GameObject.FindGameObjectWithTag("GameController");
        gameObject.GetComponents<GameManager>();
        DateTime d = DateTime.Now;
        _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
            + "_" + SceneManager.GetActiveScene().name + "_";
        _recordFiles = new List<StreamWriter>();
#if UNITY_EDITOR
        _prefix = "Output\\" + _prefix;
#else
            _prefix = Application.dataPath + "\\Output\\" + _prefix;
#endif
    }
    // Use this for initialization
    void Start()
    {
        //Debug.Log("Call Start");
        _levelTime = Time.realtimeSinceStartup;

        _sceneStatistics = File.CreateText(_prefix + "SceneStatistics" + ".txt");
        _sceneStatistics.WriteLine("Scene name:\t" + SceneManager.GetActiveScene().name);

        if (UnityEngine.XR.XRSettings.enabled)
            _sceneStatistics.WriteLine("In VR mode");
        else
            _sceneStatistics.WriteLine("In VE mode");

        _sceneStatistics.WriteLine("Camera FOV:\t" + Camera.main.fieldOfView);

        _sceneStatistics.WriteLine("------------------------");
        _sceneStatistics.WriteLine("Parameters:");
        _sceneStatistics.WriteLine("perceptionThresholdForObject\t\t" + Parameters.gazeTimeThresholdForObject);
        //_sceneStatistics.WriteLine("outGazeTimeThreshold\t\t"+Parameters.outGazeTimeThreshold);
        _sceneStatistics.WriteLine("gazeAngleThreshold\t\t" + Parameters.gazeAngleThreshold);
        //_sceneStatistics.WriteLine("gazeTimeThresholdForCamera\t\t"+Parameters.gazeTimeThresholdForCamera);
        _sceneStatistics.WriteLine("temporary out of range time limite:\t\t" + Parameters.outGazeTimeThreshold);


        _sceneStatistics.WriteLine("------------------------");
        _sceneStatistics.WriteLine("Perception values are recorded at the end of this file:");

        _sceneStatistics.WriteLine("------------------------");
        _sceneStatistics.WriteLine("Included angles for each object at each fixed frame:");

        if (!allObjects)
        {
            allObjects = GameObject.FindGameObjectWithTag("AllObjects");
        }
        if (allObjects)
        {
            _sceneStatistics.Write("Time\t");
            allRecorders = allObjects.GetComponentsInChildren<Recorder>();
            foreach (Recorder r in allRecorders)
            {
                _sceneStatistics.Write(r.name + "\t");
            }
            _sceneStatistics.WriteLine();
        }


    }
    void FixedUpdate()
    {
        if (!allObjects || !allObjects.activeSelf)
            return;

        _sceneStatistics.Write(Time.timeSinceLevelLoad.ToString("F"));
        _sceneStatistics.Write("\t");

        foreach (Recorder r in allRecorders)
        {
            _sceneStatistics.Write(r.IncludedAng.ToString("F") + "\t");
        }
        _sceneStatistics.WriteLine();
    }
    // provide an interface for calling, create a particular named file for it
    public StreamWriter GetFile(string filename)
    {
        StreamWriter f = File.CreateText(_prefix + filename + ".txt");
        _recordFiles.Add(f);
        return f;
    }
    // Quit first, Destroy second
    void OnApplicationQuit()
    {
    }
    void OnDisable()
    {
    }
    // Make sure close files last
    void OnDestroy()
    {
        for (int i = 0; i < _recordFiles.Count; i++)
            _recordFiles[i].Close();
        _sceneStatistics.Flush();
        _sceneStatistics.Close();
    }
}
