﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parameters : MonoBehaviour {
    // output screen image if player is gazing at me
    // play with the empirical values
    public const float gazeTimeThresholdForObject = 3.0f;       // can be indicated as gazing time
    public const float outGazeTimeThreshold = 0.5f;    // temperately out of range during gazing
    public const float gazeAngleThreshold = 15.0f;     // the threshold angle degree can be seen as gazing

    public const float gazeTimeThresholdForCamera = 5.0f;       // how long time a smooth moving viewpoint can be seen as gazing

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
