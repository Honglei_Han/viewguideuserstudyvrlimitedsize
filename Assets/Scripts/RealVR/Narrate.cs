﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Narrate : MonoBehaviour {
    public CharacterCtrl[] characters;
    public float showTime = 100.0f;

    private float endTime = 0.0f;
	// Use this for initialization
	void Start () {		
        foreach (CharacterCtrl c in characters)
        {
            c.gameObject.SetActive(false);
            StartCoroutine(ActiveCharacter(c));
        }
        endTime = showTime;
	}
    IEnumerator ActiveCharacter(CharacterCtrl c)
    {
        yield return new WaitForSeconds(c.appearTime);
        c.gameObject.SetActive(true);
    }
	// Update is called once per frame
	void Update () {
        endTime -= Time.deltaTime;
        if (endTime < 0.0f)
        {
            Application.Quit();
        }
	}
}
