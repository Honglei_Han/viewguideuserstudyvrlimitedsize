﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

//enum UserStatus { Normal, Gaze, Turn }

public class UserGaze : MonoBehaviour
{
    /*
    private const float gazeAngleSpeedThreshold = 2.0f / 1.0f;

    private const float turnAngleSpeedThreshold = 3.0f / 1.0f;
    private const float turnTimeThreshold = 2.0f;

    private float smoothedAngleSpeed = 0.0f;            // use average angle speed in smoothFactor frames
    private const int smoothFactor = 10;                // how many frames to smooth
    private int currentFrame = 0;
    private int frameSpeedInterval = 5;

    private float turnTime = 0.0f;
    private Quaternion beginGazeAngle;
    private Quaternion beginTrunAngle;
    private Quaternion preframeAngle;
    */

    private float gazeTime = 0.0f;                      // current smooth moving time after first frame of not moving large
    private Quaternion[] previousViews;                 // views in frames of last half gazeTimeThreshold time 
    private int previousN = 0;                          // how many frames in previousViews

    //UserStatus userStatus = UserStatus.Normal;

    // Use this for initialization
    void Start()
    {
        //preframeAngle = transform.rotation;
        previousN = (int)(0.5f * Parameters.gazeTimeThresholdForCamera / 0.02);
        previousViews = new Quaternion[previousN];
        for (int i = 0; i < previousN; i++)
        {
            previousViews[i] = transform.rotation;
        }
    }

    bool OutofRangeWithPrevious(Quaternion currentQ)
    {
        float maxAngle = 0.0f;
        for (int i = 0; i < previousN; i++)
        {
            float a = Quaternion.Angle(currentQ, previousViews[i]);
            if (a > maxAngle)
                maxAngle = a;
            if (i != previousN - 1)
                previousViews[i] = previousViews[i + 1];
        }
        previousViews[previousN - 1] = currentQ;
        if (maxAngle > Parameters.gazeAngleThreshold)
            return true;
        else
            return false;
    }
    bool GazingUseAngle()
    {
        // not use camer gaze, but only object based gaze [3/27/2017 Han]
        return false;
        //////////////////////////////////////////////////////////////////////////

        if (!OutofRangeWithPrevious(transform.rotation))
            gazeTime += Time.deltaTime;
        else
            gazeTime = 0.0f;

        if (gazeTime > 0.5 * Parameters.gazeTimeThresholdForCamera)
        {
            DateTime d = DateTime.Now;
            string _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
                + "_" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "_";
#if UNITY_EDITOR
            _prefix = "Output\\" + _prefix;
#else
            _prefix = Application.dataPath + "\\Output\\" + _prefix;
#endif

            ScreenCapture.CaptureScreenshot(_prefix + "_Player_GazeScreen_Frame_" + Time.frameCount + ".png", 1);
            gazeTime = 0.0f;
            return true;
        }
        else
            return false;
    }
    // Update is called once per frame
    void Update()
    {
    }
    void FixedUpdate()
    {
        Debug.Log(Time.deltaTime);
        //GetAngleSpeed();
        //GazingUseSpeed();
        GazingUseAngle();
        //Turning();
    }
}
    /*
    float GetAngleSpeed()
    {
        float angleSpeed = Quaternion.Angle(transform.rotation, preframeAngle) / Time.deltaTime;
        preframeAngle = transform.rotation;

        if (currentFrame < smoothFactor)
        {
            smoothedAngleSpeed = (smoothedAngleSpeed * currentFrame + angleSpeed) / (currentFrame + 1);
            currentFrame++;
        }
        else
            smoothedAngleSpeed = (smoothedAngleSpeed * (smoothFactor - 1) + angleSpeed) / smoothFactor;
        return smoothedAngleSpeed;
    }
 
    bool GazingUseSpeed()
    {
        if (smoothedAngleSpeed < gazeAngleSpeedThreshold && userStatus == UserStatus.Gaze)
        {
            gazeTime += Time.deltaTime;
        }
        else if (userStatus != UserStatus.Gaze)
        {
            userStatus = UserStatus.Gaze;
            gazeTime = 0.0f;
        }
        else if (userStatus == UserStatus.Gaze)
        {
            userStatus = UserStatus.Normal;
            gazeTime = 0.0f;
        }

        if (gazeTime > gazeTimeThreshold)
        {
            DateTime d = DateTime.Now;
            string _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
                + "_" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "_";
            Application.CaptureScreenshot("Output\\" + _prefix + "_Player_GazeScreen_Frame_" + Time.frameCount + ".png", 1);
            gazeTime = 0.0f;
            return true;
        }
        else
            return false;
    }

    bool Turning()
    {
        if (smoothedAngleSpeed > turnAngleSpeedThreshold && userStatus == UserStatus.Turn)
        {
            turnTime += Time.deltaTime;
        }
        else if (userStatus != UserStatus.Turn)
        {
            userStatus = UserStatus.Turn;
            turnTime = 0.0f;
        }
        else if (userStatus == UserStatus.Turn)
        {
            userStatus = UserStatus.Normal;
            turnTime = 0.0f;
        }

        if (turnTime > gazeTimeThreshold)
            return true;

        else
            return false;

    }
}
*/