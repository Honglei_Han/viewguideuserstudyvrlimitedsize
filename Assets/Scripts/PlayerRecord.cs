﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

/*
 * Make sure to grab this script to main Camera, maybe is the child of 1st person controller
 * Don't directly grab it into 1st person controller, as it doesn't have freedom degree of tilt
*/
public class PlayerRecord : MonoBehaviour
{
    private StreamWriter sr = null;
    private float stayTime;
    public OutputFiles outputFiles = null;
    // Use this for initialization
    void Start()
    {
        if (!outputFiles)
        {
            GameObject lm = GameObject.FindGameObjectsWithTag("GameController")[0];
            outputFiles = lm.GetComponent<OutputFiles>();
        }
        Debug.Assert(outputFiles);
        sr = outputFiles.GetFile("Camera" + transform.position);

        sr.WriteLine(name);
        sr.WriteLine("Player behavior record");
        sr.WriteLine("Screen size(width,height):\t" + Screen.width + ",\t" + Screen.height);
        // information about how to indicate gazing to some directions
        sr.WriteLine("Gaze time threshold: " + Parameters.gazeTimeThresholdForCamera);       // can be indicated as gazing time
        sr.WriteLine("How narrow of angle can be seen as gazing: " + Parameters.gazeAngleThreshold);     // the threshold angle degree can be seen as gazing

        sr.WriteLine("Current time, Player position, and Player orientation");
        stayTime = Time.realtimeSinceStartup;
        //Debug.Log("Call Start");

    }
    void OnDisable()
    {
        sr.Write("Player elapse time \t");
        sr.WriteLine(Time.realtimeSinceStartup - stayTime);
    }
    // clamp the rotation angle into -180~180
    string ClampAngle(float a)
    {
        float aa = a;
        if (a > 180.0f)
        {
            aa -= 360.0f;
        }
        return aa.ToString("F");
    }

    void FixedUpdate()
    {
        sr.Write(Time.timeSinceLevelLoad.ToString("F"));
        sr.Write("\t\t");
        sr.Write(transform.position.x.ToString("F"));
        sr.Write("\t");
        sr.Write(transform.position.y.ToString("F"));
        sr.Write("\t");
        sr.Write(transform.position.z.ToString("F"));
        sr.Write("\t\t\t");
        sr.Write(ClampAngle(transform.rotation.eulerAngles.x));
        sr.Write("\t");
        sr.Write(ClampAngle(transform.rotation.eulerAngles.y));
        sr.Write("\t");
        sr.WriteLine(ClampAngle(transform.rotation.eulerAngles.z));
        //if (Time.time > timeOut || Input.GetKeyUp(KeyCode.Escape))
        //    Application.Quit();
    }
}
