﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using System;

public class Recorder : MonoBehaviour
{
    /*
      How to use:
      Grab a 1st Person Controller to the scene
      * Set collider component to the object recorded
      * Give a meaningful name to this object
      * Grab this script to this object
      * Run the game, and let the player to navigate game scene freely
      * Quit the game, all recorded data files will be saved in the folder named Output
      */
    [SerializeField]
    protected Camera _mainCam;
    public OutputFiles outputFiles = null;

    protected StreamWriter sr;                // record file stream
    // record included angle into level statistics file instead [4/3/2017 Han]
    //protected StreamWriter sr_includeAng;     // this file only record included angle
    protected float stayTime;                 // the time stayed in the view frustum
    protected float appTime;                  // the time after the game started
    protected bool bVisible;                  // is this object visible

    protected bool bFirstV = false;           // is this object first be observed

    protected double perception = 0.0;        // perception value use the euqation 1/(1+EXP(0.5*(ang-15)))
    private double perceptionAccum = 0.0f;
    // test purpose, to evaluate the perception thresholds [5/4/2017 Han]
    private double perceptionAccumS = 0.0f;     // use small threshold
    private double perceptionAccumL = 0.0f;     // use large threshold

    private float outGazeTime = 0.0f;

    private int outImages = 1;

    private float includedAng = 0.0f;
    public float IncludedAng
    {
        get { return includedAng; }
    }

    // Use this for initialization
    protected void Start()
    {
        if (!outputFiles)
        {
            GameObject lm = GameObject.FindGameObjectWithTag("GameController");
            outputFiles = lm.GetComponent<OutputFiles>();
        }
        Debug.Assert(outputFiles);
        sr = outputFiles.GetFile(name + transform.position);    // get the record file pointer//
        //sr_includeAng = outputFiles.GetFile(name + "_IncludedAngle_" + transform.position);

        sr.Write("Centric of object\t");
        sr.WriteLine(transform.position.x + "\t" + transform.position.y + "\t" + transform.position.z);// -xcc 12.22
        stayTime = 0.0f;
        appTime = Time.realtimeSinceStartup;

        if (_mainCam == null)       // get main camera
            _mainCam = Camera.main;

        // record field of view of the camera
        sr.WriteLine("Camera field of view is: " + _mainCam.fieldOfView);

        // information of gaze parameters
        sr.WriteLine("Gaze time threshold: " + Parameters.gazeTimeThresholdForObject);       // can be indicated as gazing time
        sr.WriteLine("Temporary out of range time threshold when gazing: " + Parameters.outGazeTimeThreshold);    // temperately out of range during gazing
        sr.WriteLine("How narrow of angle can be seen as gazing to this object:" + Parameters.gazeAngleThreshold);     // the threshold angle degree can be seen as gazing


        // information head
        sr.WriteLine();
        sr.WriteLine("Gaze info record");
        sr.WriteLine("Duration\t\tDist to cam\t\tObj cet in scrn\t\tPos in cam\t\tWorld pos\t\tRot in cam\t\tWorld rot\t\tInclude Ang");

        //sr_includeAng.WriteLine(name + "@" + SceneManager.GetActiveScene().name);
    }

    protected void FixedUpdate()
    {
        if (sr == null /*|| sr_includeAng == null*/)
        {
            Debug.Log(name + "file Error!");
            return;
        }
        Collider cld = GetComponent<Collider>();

        // the angle between camera direction and the vector from camera to this object
        //Vector3 link = transform.position - _mainCam.transform.position;
        // use collider position
        Vector3 link;
        if (!cld)
            link = transform.position - _mainCam.transform.position;
        else
            link = cld.bounds.center - _mainCam.transform.position;
        includedAng = Vector3.Angle(_mainCam.transform.forward, link);


        // record included angles only into this particular file
        //sr_includeAng.WriteLine(/*Time.time.ToString("F") + "\t" + */includedAng.ToString("F"));

        bVisible = IsRenderedFrom(transform, _mainCam);

        // current time
        sr.Write(/*name + "\t" + */Time.timeSinceLevelLoad.ToString("F"));
        sr.Write("\t\t");

        // if this object is invisible, just return
        if (!bVisible)//!this.renderer.isVisible ||
        {
            if (bFirstV)
                bFirstV = false;					// han
            sr.WriteLine("Invisible");

            return;
        }

        // accumulate perception value [3/27/2017 Han]
        double p = Time.deltaTime * 1.0 / (1.0 + Mathf.Exp(0.5f * (includedAng - Parameters.gazeAngleThreshold)));
        perception += p;
        perceptionAccum += p;
        perceptionAccumS += p;
        perceptionAccumL += p;

        Vector3 screenPos = _mainCam.WorldToScreenPoint(transform.position);

        stayTime += Time.deltaTime;		// total time to stay visible

        float dist = (_mainCam.transform.position - transform.position).magnitude;
        // distance to the camera
        sr.Write(dist.ToString("F"));
        sr.Write("\t\t");
        // the center of the object screen coordinate, between -0.5 to 0.5
        var xx = 0.5 - screenPos.x / _mainCam.pixelWidth;
        var yy = 0.5 - screenPos.y / _mainCam.pixelHeight;

        sr.Write(xx.ToString("F"));
        sr.Write("\t");
        sr.Write(yy.ToString("F"));
        sr.Write("\t\t");

        // object position in camera coordinate [10/28/2016 Han]
        Vector3 camCoodPos = _mainCam.transform.InverseTransformPoint(transform.position);
        sr.Write(camCoodPos.x.ToString("F") + "\t" + camCoodPos.y.ToString("F") + "\t" + camCoodPos.z.ToString("F"));
        sr.Write("\t\t");

        // World position [11/1/2016 Han]
        sr.Write(transform.position.x.ToString("F") + "\t" + transform.position.y.ToString("F") + "\t" + transform.position.z.ToString("F"));
        sr.Write("\t\t");

        // object rotation in camera coordinate [11/1/2016 Han]
        Vector3 camCoodRot = _mainCam.transform.InverseTransformPoint(transform.rotation.eulerAngles);
        sr.Write(camCoodRot.x.ToString("F") + "\t" + camCoodRot.y.ToString("F") + "\t" + camCoodRot.z.ToString("F"));
        sr.Write("\t\t");

        // World object rotation [11/1/2016 Han]
        sr.Write(transform.rotation.eulerAngles.x.ToString("F") + "\t" + transform.rotation.eulerAngles.y.ToString("F") + "\t" + transform.rotation.eulerAngles.z.ToString("F"));
        sr.Write("\t\t");

        // the angle between camera direction and the vector from camera to this object
        sr.Write(includedAng.ToString("F") + "\t\t");

        if (!bFirstV)
        {
            bFirstV = true;
        }
        sr.WriteLine();
        //  [4/4/2017 Han]
        IsGazingLongtime();
    }

    bool IsGazingLongtime()
    {
        /************************************************************************/
        /* Use the perception equation to indicate if the perception is exceeding a particular value 
        //  [3/27/2017 Han]
        if (perception > outImages * Parameters.gazeTimeThresholdForObject)
        {
            outImages++;
            DateTime d = DateTime.Now;
            string _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
                + "_" + SceneManager.GetActiveScene().name + "_";
#if UNITY_EDITOR
            _prefix = "Output\\" + _prefix;
#else
            _prefix = Application.dataPath + "\\Output\\" + _prefix;
#endif
            Application.CaptureScreenshot(_prefix + gameObject.name + "_GazeScreen_Frame_" + Time.frameCount + ".png", 1);
            currentGazeTime = 0.0f;
            outGazeTime = 0.0f;
            return true;
        }
        return false;
        /************************************************************************/


        if (IncludedAng < Parameters.gazeAngleThreshold)
        {
            outGazeTime = 0.0f;
        }
        else 
        {
            // if out of range for a long time means not gazing any longer
            outGazeTime += Time.deltaTime;
            if (outGazeTime > Parameters.outGazeTimeThreshold)
            {
                perceptionAccum = 0.0;
                perceptionAccumL = 0.0;
                perceptionAccumS = 0.0;
            }
        }

        if (perceptionAccum > Parameters.gazeTimeThresholdForObject)
        {
            DateTime d = DateTime.Now;
            string _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
                + "_" + SceneManager.GetActiveScene().name + "_";
#if UNITY_EDITOR
            _prefix = "Output\\" + _prefix;
#else
            _prefix = Application.dataPath + "\\Output\\" + _prefix;
#endif

            ScreenCapture.CaptureScreenshot(_prefix + gameObject.name + "_MID_GazeScreen_Frame_" + Time.frameCount + ".png", 1);
            perceptionAccum = 0.0;
            return true;
        }
        if (perceptionAccumS > Parameters.gazeTimeThresholdForObject-1)
        {
            DateTime d = DateTime.Now;
            string _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
                + "_" + SceneManager.GetActiveScene().name + "_";
#if UNITY_EDITOR
            _prefix = "Output\\" + _prefix;
#else
            _prefix = Application.dataPath + "\\Output\\" + _prefix;
#endif

            ScreenCapture.CaptureScreenshot(_prefix + gameObject.name + "_SMALL_GazeScreen_Frame_" + Time.frameCount + ".png", 1);
            perceptionAccumS = 0.0;
            return true;
        }
        if (perceptionAccumL > Parameters.gazeTimeThresholdForObject+1)
        {
            DateTime d = DateTime.Now;
            string _prefix = d.Year + "_" + d.Month + "_" + d.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second
                + "_" + SceneManager.GetActiveScene().name + "_";
#if UNITY_EDITOR
            _prefix = "Output\\" + _prefix;
#else
            _prefix = Application.dataPath + "\\Output\\" + _prefix;
#endif

            ScreenCapture.CaptureScreenshot(_prefix + gameObject.name + "_LARGE_GazeScreen_Frame_" + Time.frameCount + ".png", 1);
            perceptionAccumL = 0.0;
            return true;
        }
        
        return false;
    }

    void OnDisable()
    {
        sr.Write("Total gaze duration of this object \t" + stayTime + "\t");
        sr.WriteLine("Duration percentage \t" + stayTime / (Time.realtimeSinceStartup - appTime));
        sr.WriteLine("This object exists total time \t" + (Time.realtimeSinceStartup - appTime));
        sr.WriteLine("The perception of this object is \t" + perception);

        outputFiles._sceneStatistics.WriteLine(name + "\t" + transform.position + "\t \t \t" + perception);
        outputFiles._sceneStatistics.Flush();
    }

    void OnApplicationQuit()
    {
        sr.Write("Total gaze duration of this object \t" + stayTime + "\t");
        sr.WriteLine("Duration percentage \t" + stayTime / (Time.realtimeSinceStartup - appTime));
        sr.WriteLine("This object exists total time \t" + (Time.realtimeSinceStartup - appTime));
        sr.WriteLine("The perception of this object is \t" + perception);

        outputFiles._sceneStatistics.WriteLine(name + "\t" + transform.position + "\t \t \t" + perception);
        outputFiles._sceneStatistics.Flush();
    }

    // as some objects has no renderer component, so use geometric method to judge whether it's visible or not
    static bool IsRenderedFrom(Transform trans, Camera camera)
    {
        Collider cld = trans.gameObject.GetComponent<Collider>();

        var planes = GeometryUtility.CalculateFrustumPlanes(camera);
        if (!cld)
        {
            Vector3 link = trans.position - camera.transform.position;
            float ang = Vector3.Angle(camera.transform.forward, link);
            return (ang < camera.fieldOfView / 2.0f);
        }
        else
            return GeometryUtility.TestPlanesAABB(planes, cld.bounds);
    }
}
