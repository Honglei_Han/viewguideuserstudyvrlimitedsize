﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // required when using UI elements in scripts
using UnityEngine.EventSystems;
using System.IO;
using UnityEngine.SceneManagement;
using System;// Required when using Event data.

public class UserInputs : MonoBehaviour
{
    private InputField myInputField;        // input field for users
    private StreamWriter sr;                // record users' answer into this file
    public OutputFiles outputFiles = null;
    private List<Vector2> allMath;

    // Use this for initialization
    void Start()
    {
        if (!outputFiles)
        {
            GameObject lm = GameObject.FindGameObjectWithTag("GameController");
            outputFiles = lm.GetComponent<OutputFiles>();
        }
        Debug.Assert(outputFiles);
        sr = outputFiles.GetFile("UserAnswer");
        allMath = new List<Vector2>();

    }
    void Awake()
    {
        myInputField = gameObject.GetComponentInChildren<InputField>();
        myInputField.Select();
        //Debug.Log(myInputField);
    }
    // call this function automatically when the user answer UI becomes enable
    void OnEnable()
    {
        myInputField.text = "Your answer";
        EventSystem.current.SetSelectedGameObject(myInputField.gameObject);
        myInputField.ActivateInputField();
    }
    // called by GameManager when the answer UI is enable, to record the math problems in this scene
    public void BeginAnswer()
    {
        sr.WriteLine("The math problems are:");
        GameObject g = GameObject.FindGameObjectWithTag("AllObjects");
        if (!g)
        {
            sr.WriteLine("No valid objects");
            return;
        }
        RecordPerception[] objs = g.GetComponentsInChildren<RecordPerception>();
        foreach (RecordPerception o in objs)
        {
            if (!o.GetComponentInChildren<Text>())
                continue;
            sr.Write(o.name);
            foreach (Vector2 v in o.MathProblems)
            {
                sr.Write("\t" + v.ToString("0.##"));
                allMath.Add(v);
            }
            sr.WriteLine();
        }
        sr.WriteLine();
    }
    // record user's answer into the file
    // and change to the next scene
    public void InputDone(InputField input)
    {
        Debug.Log("Input done");
        sr.Write("User answer is:\t");
        sr.WriteLine(input.text);

        AnswerCorrect(input.text);

        GameObject lm = GameObject.FindGameObjectsWithTag("GameController")[0];
        GameManager g = lm.GetComponent<GameManager>();
        g.PlayNextScene();
    }
    // calculate user answer's correct percentage
    void AnswerCorrect(string answer)
    {
        string[] a = answer.Split();
        int c = 0;
        int n = allMath.Count;
        foreach (string aa in a)
        {
            foreach (Vector2 v in allMath)
            {
                int aai = -1;
                Int32.TryParse(aa, out aai);
                if (aai == (int)(v.x + v.y))
                {
                    c++;
                    allMath.Remove(v);
                    break;
                }
            }
        }
        float f = (float)c * 100.0f / n;
        sr.WriteLine("User answer accuracy is:\t" + f.ToString("F") + "%");

        outputFiles._sceneStatistics.WriteLine("------------------------");
        outputFiles._sceneStatistics.WriteLine("User answer accuracy is\t\t" + f.ToString("F") + "%");
    }
    // Update is called once per frame
    void Update()
    {

    }
}
